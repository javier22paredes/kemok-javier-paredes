from sqlalchemy import create_engine

class DataBase(object):

	

	def __init__(self):
		self.engine = create_engine('sqlite:///tasa_cambio.db')


	def crear_tabla(self):
		sql = "CREATE TABLE IF NOT EXISTS tasas("
		sql += "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
		sql += " fecha DATE NOT NULL,"
		sql += " compra REAL NOT NULL," 
		sql += " venta REAL NOT NULL);" 

		conn = self.engine.connect()
		conn.execute(sql)
		conn.close()


	def cant_registros(self):
		sql = "SELECT count(*) FROM tasas;"

		conn = self.engine.connect()
		rs = conn.execute(sql)
		total = rs.fetchone()[0]
		conn.close()
		return(total)

	def cant_registros_fecha(self,fecha_):
		sql = "SELECT count(*) FROM tasas"
		sql += " WHERE fecha >= '" + str(fecha_) + "';"

		conn = self.engine.connect()
		rs = conn.execute(sql)
		total = rs.fetchone()[0]
		conn.close()
		return(total)


	def insertar(self,valores):
		self.crear_tabla()
		sql = "INSERT INTO tasas("
		sql += "fecha,compra,venta)"
		sql +=  " VALUES " + valores + ";"

		conn = self.engine.connect()
		conn.execute(sql)
		conn.close()


	def borrar_registros_fecha(self,fecha_):
		sql = "DELETE FROM tasas"
		sql += " WHERE fecha >= '" + str(fecha_) + "';"

		conn = self.engine.connect()
		conn.execute(sql)
		conn.close()
		
